import styled from "styled-components";

export const SimonBlocContainer = styled.div`
    width:32%;
    height:32%;
    background-color:blue;
    border:solid 1px black;
    cursor:pointer;
    &:hover{
        background-color:darkblue;
    }
`;
